package com.dplaninic.cchallenge.persistence;

import com.dplaninic.cchallenge.model.Email;
import com.dplaninic.cchallenge.model.EmailBatch;
import com.dplaninic.cchallenge.model.EmailDto;
import com.dplaninic.cchallenge.model.EmailIdentity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmailRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EmailRepository emailRepository;
    private static final String path = "src/main/resources/test_load.json";

    private String loadFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    private EmailBatch loadEmailBatchFromString() throws IOException {
        String content = loadFromFile(path);
        return new EmailBatch(false, content);
    }


    @Test
    public void whenFindBySent_thenReturnEmail() throws IOException {

        EmailDto emailDto = loadEmailBatchFromString().getDto();
        Set<String> recipients = emailDto.filterAllRecipients();

        for (String recipient : recipients) {
            EmailIdentity identity = new EmailIdentity(1, emailDto.from, recipient);
            Email email = new Email(identity, emailDto.subject, emailDto.body);
            entityManager.persist(email);
            entityManager.flush();
        }

        List<Email> unsent = emailRepository.findBySent(false);

        assertTrue(unsent.size() == 2);
        assertTrue(unsent.get(0).isSent() == false);

        List<Email> sent = emailRepository.findBySent(true);

        assertTrue(sent.size() == 0);


    }

}
