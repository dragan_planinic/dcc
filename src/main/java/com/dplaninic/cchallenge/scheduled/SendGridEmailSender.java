package com.dplaninic.cchallenge.scheduled;

// using SendGrid's Java Library
// https://github.com/sendgrid/sendgrid-java

import com.dplaninic.cchallenge.model.Email;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class SendGridEmailSender implements EmailSender {

    @Autowired
    private Environment env;
    private static final Logger logger = LoggerFactory.getLogger(SendGridEmailSender.class);

    public SendGridEmailSender() {
    }

    private Mail createEmail(String from, String subject, String to, String content) {
        return new Mail(new com.sendgrid.helpers.mail.objects.Email(from),
                subject,
                new com.sendgrid.helpers.mail.objects.Email(to),
                new Content("text/plain", content));
    }

    private void sendBatch(String from, Set<String> toSet, String subject, String content) {
        for (String to : toSet) {
            sendEmail(createEmail(from, to, subject, content));
        }
    }

    private void sendEmail(Mail mail) {

        SendGrid sg = new SendGrid(env.getProperty("sendgrid.api.key"));
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    public List<Email> sendEmails(List<Email> emailList) {

        List<Email> sent = new ArrayList<>();
        for (Email entity : emailList) {
            Mail sgMail = createEmail(entity.getFrom(),
                    entity.getSubject(),
                    entity.getTo(),
                    entity.getContent());
            sendEmail(sgMail);
            entity.setSent(true);
            sent.add(entity);
        }
        return sent;
    }
}
