package com.dplaninic.cchallenge.service;

import com.dplaninic.cchallenge.model.Email;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface EmailService {

    void saveAll(final List<Email> emails);

    List<Email> getAllUnsent();


}