package com.dplaninic.cchallenge.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;

@Entity
public class EmailBatch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private boolean expanded;
    @Column(length = 5000)
    private String content;
    @Transient
    private final ObjectMapper mapper = new ObjectMapper();


    public EmailBatch() {
    }

    public EmailBatch(boolean expanded, String content) {
        this.expanded = expanded;
        this.content = content;
    }

    public EmailBatch(EmailDto email) throws JsonProcessingException {
        this.expanded = false;
        this.content = mapper.writeValueAsString(email);
    }

    public EmailDto getDto() throws JsonProcessingException {
        return this.mapper.readValue(this.content, EmailDto.class);
    }

    public int getId() {
        return id;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

}
