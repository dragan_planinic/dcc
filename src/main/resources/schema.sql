CREATE TABLE IF NOT EXISTS email_batch (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  expanded BOOLEAN DEFAULT FALSE,
  content VARCHAR(5000) NOT NULL,
);


CREATE TABLE IF NOT EXISTS email (
  from_field VARCHAR(80) NOT NULL,
  to_field VARCHAR(80) NOT NULL,
  batch_id INT,
  subject VARCHAR(250),
  content VARCHAR(5000),
  sent BOOLEAN DEFAULT FALSE,
  PRIMARY KEY(batch_id, from_field, to_field),
  CONSTRAINT FK_EMAIL_BATCH_ID FOREIGN KEY (batch_id) REFERENCES email_batch(id)
);