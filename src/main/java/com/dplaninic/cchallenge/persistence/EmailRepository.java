package com.dplaninic.cchallenge.persistence;

import com.dplaninic.cchallenge.model.Email;
import com.dplaninic.cchallenge.model.EmailIdentity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailRepository extends CrudRepository<Email, EmailIdentity> {

    List<Email> findBySent(boolean sent);

}
