package com.dplaninic.cchallenge.service;

import com.dplaninic.cchallenge.model.Email;
import com.dplaninic.cchallenge.model.EmailBatch;
import com.dplaninic.cchallenge.persistence.EmailBatchRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class EmailBatchServiceImplTest {

    @TestConfiguration
    static class EmailBatchServiceImplTestContextConfiguration {

        @Bean
        public EmailBatchService emailService() {
            return new EmailBatchServiceImpl();
        }
    }
    private static final String path = "src/main/resources/test_load.json";

    private String loadFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    private EmailBatch loadEmailBatchFromString() throws IOException {
        String content = loadFromFile(path);
        return new EmailBatch(false, content);
    }

    @Before
    public void setUp() throws IOException {
        EmailBatch emailBatch = loadEmailBatchFromString();
        List<EmailBatch> batches = new ArrayList<EmailBatch>();
        batches.add(emailBatch);

        Mockito.when(emailBatchRepository.findByExpanded(false))
                .thenReturn(batches);
    }

    @Autowired
    private EmailBatchService emailBatchService;

    @MockBean
    private EmailBatchRepository emailBatchRepository;

    @Test
    public void whenFindByUnexpanded_thenReturnEmailBatch() {
        List<EmailBatch> batches = emailBatchService.getAllUnexpanded();

        assertEquals(batches.size(),1);
    }


}