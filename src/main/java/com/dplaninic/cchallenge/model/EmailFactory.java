package com.dplaninic.cchallenge.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmailFactory {

    public EmailFactory() {
    }

    public List<Email> createEmailEntities(EmailDto emailDto, EmailBatch batch) {

        List<Email> emails = new ArrayList<>();
        emailDto.filterAllRecipients().forEach(recipient -> {
            EmailIdentity identity = new EmailIdentity(batch.getId(), emailDto.from, recipient);
            Email emailEntity = new Email(identity, emailDto.subject, emailDto.body);
            emails.add(emailEntity);
        });

        return emails;
    }
}
