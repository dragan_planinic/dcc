package com.dplaninic.cchallenge.persistence;

import com.dplaninic.cchallenge.model.EmailBatch;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmailBatchRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EmailBatchRepository emailBatchRepository;
    private static final String path = "src/main/resources/test_load.json";

    private String loadFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    private EmailBatch loadEmailBatchFromString() throws IOException {
        String content = loadFromFile(path);
        return new EmailBatch(false, content);
    }


    @Test
    public void whenFindByUnexpanded_thenReturnEmailBatch() throws IOException {

        EmailBatch emailBatch = loadEmailBatchFromString();
        entityManager.persist(emailBatch);
        entityManager.flush();
        List<EmailBatch> batches = emailBatchRepository.findByExpanded(emailBatch.isExpanded());

        assertTrue(batches.size() == 1);
        assertTrue(batches.get(0).isExpanded() == false);

        String testStringNoWS = loadFromFile(path).replaceAll("\\s+","");
        String dto = new ObjectMapper().writeValueAsString(batches.get(0).getDto());
        String dtoNoWS = dto.replaceAll("\\s+","");
        assertEquals(dtoNoWS, testStringNoWS);
    }

}
