package com.dplaninic.cchallenge.service;

import com.dplaninic.cchallenge.model.EmailBatch;
import com.dplaninic.cchallenge.persistence.EmailBatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EmailBatchServiceImpl implements EmailBatchService {

    @Autowired
    EmailBatchRepository repository;

    public void save(final EmailBatch emailBatch) {

        repository.save(emailBatch);
    }

    public List<EmailBatch> getAllUnexpanded() {

        return repository.findByExpanded(false);
    }
}