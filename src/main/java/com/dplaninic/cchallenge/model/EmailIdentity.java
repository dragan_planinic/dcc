package com.dplaninic.cchallenge.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class EmailIdentity implements Serializable {

    @Column(name = "from_field")
    private String from;
    @Column(name = "batch_id")
    private int batchId;
    @Column(name = "to_field")
    private String to;

    public EmailIdentity() {
    }

    public EmailIdentity(int batchId, String from, String to) {
        this.from = from;
        this.batchId = batchId;
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public int getBatchId() {
        return batchId;
    }

    public String getTo() {
        return to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmailIdentity that = (EmailIdentity) o;

        if (!from.equals(that.from)) {
            return false;
        }
        if (!to.equals(that.to)) {
            return false;
        }
        return batchId == that.batchId;
    }

    @Override
    public int hashCode() {
        int result = from.hashCode();
        return 31 * result + to.hashCode() + batchId;
    }
}