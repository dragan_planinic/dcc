# README #

Coding challenge application for Deloitte Digital Core AT 

### Design ###

The app sits between email protocols and HTTP. Due to the non-functional requirement
that emails be guaranteed to be delivered, the main design idea is this:

- Save the POST body request in the database as JSON.
- Return 20X HTTP code to user on a successful saving of the request to the database.
- Transform POST body JSON to single, unsent email entities in the database.
- Continuously check for unsent mails in the database.
- Mark mails as sent when the external service (SendGrid) sends real emails.
- The idea is that if the external service is unavailable, 
  the emails will wait in the database for it to come online, therefore guaranteeing the delivery**

** App runs on a in-memory database due to time-constraints :).

### Deployment ###

The app is deployed to Heroku. It's available on https://shielded-refuge-20359.herokuapp.com/.
Inside of this repository there is also a simple Python script ```deploy.py``` that acts as a CI/CD pipeline.
It pulls the head of the master branch, runs ```mvn clean dependency:list install``` which will also run tests,
as well as packaging jar in target folder. The target/name-of-the-jar.jar is then deployed to Heroku with something like this:
```heroku deploy:jar target/cchallenge-0.0.1-SNAPSHOT.jar --app shielded-refuge-20359```

For this pipeline to work, mvn and heroku cli need to be installed on developer's machine.
heroku cli also demands java plugin to be installed.

### TO-DO's ###

There are, of course, several ways to improve both app and a pipeline, but some shortcuts were necessary.
The persistent database should be used on Heroku, application properties should not be shared between
test and production profile(there should be at least two profiles), ```deploy.``` should be parameterized,
secrets should be injected during the pipeline and not be unencrypted in Git repository...