package com.dplaninic.cchallenge.controller;

import com.dplaninic.cchallenge.model.Email;
import com.dplaninic.cchallenge.model.EmailBatch;
import com.dplaninic.cchallenge.persistence.EmailRepository;
import com.dplaninic.cchallenge.service.EmailBatchService;
import com.dplaninic.cchallenge.service.EmailService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class FlowIntegrationTest {

    @Autowired
    private MockMvc mvc;
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private EmailService emailService;
    @Autowired
    private EmailRepository emailRepository;
    @Autowired
    private EmailBatchService emailBatchService;

    private String loadFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    private EmailBatch loadEmailBatchFromString(String path) throws IOException {
        String content = loadFromFile(path);
        return new EmailBatch(false, content);
    }

    @Test
    public void postCreateEmailValid1() throws Exception {

        EmailBatch emailBatch = loadEmailBatchFromString("src/main/resources/json/valid/1.json");
        mvc.perform(MockMvcRequestBuilders.post("/createemail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailBatch.getDto())))
                .andExpect(status().isCreated());

        List<EmailBatch> unexpandedBefore = emailBatchService.getAllUnexpanded();
        assertEquals(unexpandedBefore.size(),1);

        Thread.sleep(40000);

        List<Email> unsent = emailService.getAllUnsent();
        List<EmailBatch> unexpandedAfter = emailBatchService.getAllUnexpanded();
        assertEquals(unsent.size(), 0);
        assertEquals(unexpandedAfter.size(),0);
    }

}