package com.dplaninic.cchallenge.service;

import com.dplaninic.cchallenge.model.Email;
import com.dplaninic.cchallenge.persistence.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    EmailRepository repository;

    public void saveAll(final List<Email> emails) {
        repository.saveAll(emails);
    }

    public List<Email> getAllUnsent() {
        return repository.findBySent(false);
    }


}