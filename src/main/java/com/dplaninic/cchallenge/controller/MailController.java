package com.dplaninic.cchallenge.controller;

import com.dplaninic.cchallenge.model.EmailBatch;
import com.dplaninic.cchallenge.model.EmailDto;
import com.dplaninic.cchallenge.service.EmailBatchServiceImpl;
import com.dplaninic.cchallenge.validation.EmailDtoValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MailController {

    @Autowired
    private Environment env;
    @Autowired
    EmailBatchServiceImpl emailBatchService;

    private static final Logger logger = LoggerFactory.getLogger(MailController.class);

    private String getValidationMessage(BindingResult bindingResult) {
        for (Object object : bindingResult.getAllErrors()) {
            if (object instanceof FieldError) {
                FieldError fieldError = (FieldError) object;
                return env.getProperty(fieldError.getCode());
            }
            if (object instanceof ObjectError) {
                ObjectError objectError = (ObjectError) object;
                return env.getProperty(objectError.getCode());
            }
        }
        return new String("");
    }

    @RequestMapping("/")
    public String index() {
        return "Running Deloitte coding challenge app on Heroku!";
    }

    @PostMapping(value = "/createemail", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> createEmail(@RequestBody EmailDto emailDto, BindingResult bindingResult) {

        new EmailDtoValidator().validate(emailDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<String>(getValidationMessage(bindingResult), HttpStatus.BAD_REQUEST);
        }
        try {
            EmailBatch entity = new EmailBatch(emailDto);
            emailBatchService.save(entity);

            return new ResponseEntity<>(emailDto, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>("Error sending emails", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}