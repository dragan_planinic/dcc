package com.dplaninic.cchallenge.service;

import com.dplaninic.cchallenge.model.Email;
import com.dplaninic.cchallenge.model.EmailBatch;
import com.dplaninic.cchallenge.model.EmailDto;
import com.dplaninic.cchallenge.model.EmailIdentity;
import com.dplaninic.cchallenge.persistence.EmailRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class EmailServiceImplTest {

    @TestConfiguration
    static class EmailServiceImplTestContextConfiguration {

        @Bean
        public EmailService emailService() {
            return new EmailServiceImpl();
        }
    }
    private static final String path = "src/main/resources/test_load.json";

    private String loadFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    private EmailBatch loadEmailBatchFromString() throws IOException {
        String content = loadFromFile(path);
        return new EmailBatch(false, content);
    }

    @Before
    public void setUp() throws IOException {
        EmailDto emailDto = loadEmailBatchFromString().getDto();
        Set<String> recipients = emailDto.filterAllRecipients();
        List<Email> emails = new ArrayList<>();

        for (String recipient : recipients) {
            EmailIdentity identity = new EmailIdentity(1, emailDto.from, recipient);
            Email email = new Email(identity, emailDto.subject, emailDto.body);
            emails.add(email);

        }
        Mockito.when(emailRepository.findBySent(false))
                .thenReturn(emails);
        Mockito.when(emailRepository.findBySent(true))
                .thenReturn(new ArrayList<Email>());
    }

    @Autowired
    private EmailService emailService;

    @MockBean
    private EmailRepository emailRepository;

    @Test
    public void whenFindBySent_thenReturnEmail() {

        List<Email> unsent = emailService.getAllUnsent();

        assertTrue(unsent.size() == 2);
        assertTrue(unsent.get(0).isSent() == false);

        List<Email> sent = emailRepository.findBySent(true);

        assertTrue(sent.size() == 0);
    }


}