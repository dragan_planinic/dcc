package com.dplaninic.cchallenge.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class Email {
    @EmbeddedId
    private EmailIdentity emailIdentity;
    private String subject;
    @Column(length = 5000)
    private String content;
    private boolean sent = false;

    public Email() {
    }

    public Email(EmailIdentity emailIdentity, String subject, String content) {
        this.emailIdentity = emailIdentity;
        this.subject = subject;
        this.content = content;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public int getBatchId() {
        return emailIdentity.getBatchId();
    }

    public String getFrom() {
        return emailIdentity.getFrom();
    }

    public String getTo() {
        return emailIdentity.getTo();
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public boolean isSent() {
        return sent;
    }
}
