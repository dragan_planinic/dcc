package com.dplaninic.cchallenge.persistence;


import com.dplaninic.cchallenge.model.EmailBatch;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EmailBatchRepository extends CrudRepository<EmailBatch, Integer> {

    List<EmailBatch> findByExpanded(boolean expanded);
}