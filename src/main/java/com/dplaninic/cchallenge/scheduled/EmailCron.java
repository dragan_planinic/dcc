package com.dplaninic.cchallenge.scheduled;

import com.dplaninic.cchallenge.model.Email;
import com.dplaninic.cchallenge.model.EmailBatch;
import com.dplaninic.cchallenge.model.EmailDto;
import com.dplaninic.cchallenge.model.EmailFactory;
import com.dplaninic.cchallenge.service.EmailBatchServiceImpl;
import com.dplaninic.cchallenge.service.EmailService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmailCron {

    @Value("${scheduling.enabled:true}")
    private boolean isSchedulingEnabled;
    @Autowired
    EmailFactory emailFactory;
    @Autowired
    EmailService emailService;
    @Autowired
    EmailBatchServiceImpl emailBatchService;
    @Autowired
    EmailSender emailSender;
    private static final Logger logger = LoggerFactory.getLogger(EmailCron.class);

    private void expandEmailBatches() {

        List<EmailBatch> unexpanded = emailBatchService.getAllUnexpanded();
        for (EmailBatch batch : unexpanded) {

            EmailDto emailDto;
            try {
                emailDto = batch.getDto();
            } catch (JsonProcessingException ex) {
                logger.error("Error parsing JSON on batch ID:" + batch.getId());
                continue;
            }

            List<Email> emailEntities = emailFactory.createEmailEntities(emailDto, batch);
            try {
                emailService.saveAll(emailEntities);
            } catch (Exception e) {
                logger.error("Error parsing JSON on batch ID:" + batch.getId());
                continue;
            }
            batch.setExpanded(true);
            emailBatchService.save(batch);

        }

    }

    @Scheduled(fixedRate = 30000)
    public void runCron() {
        if (!isSchedulingEnabled) return;
        this.sendUnsentEmails();
    }

    public void sendUnsentEmails() {

        this.expandEmailBatches();
        List<Email> unsent = emailService.getAllUnsent();
        List<Email> sent = emailSender.sendEmails(unsent);
        emailService.saveAll(sent);

    }
}
