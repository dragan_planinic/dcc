#!/usr/bin/python

import os


def get_jar():
    for file in os.listdir(os.path.dirname(__file__) + "/target"):
        if file.endswith(".jar"):
            return file


APP_NAME = "shielded-refuge-20359"

os.system("git pull origin master")
os.system("mvn clean dependency:list install")
JAR_NAME = get_jar()
os.system("heroku deploy:jar target/" + JAR_NAME + " --app " + APP_NAME)

