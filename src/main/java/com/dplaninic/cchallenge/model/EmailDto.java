package com.dplaninic.cchallenge.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class EmailDto {

    public String from;
    public String to;
    public String cc;
    public String bcc;
    public String subject;
    public String body;

    public EmailDto() {
    }

    public EmailDto(String from, String to, String cc, String bcc, String subject, String body) {
        this.bcc = bcc;
        this.to = to;
        this.body = body;
        this.subject = subject;
        this.from = from;
        this.cc = cc;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getCc() {
        return cc;
    }

    public String getBcc() {
        return bcc;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public Set<String> filterAllRecipients() {
        Set<String> set = new HashSet<String>();

        String[] to = this.to.split(",");
        if (to.length > 0 && to[0].length() != 0) {
            set.addAll(Arrays.asList(to));
        }
        String[] cc = this.cc.split(",");
        if (cc.length > 0 && cc[0].length() != 0) {
            set.addAll(Arrays.asList(cc));
        }
        String[] bcc = this.bcc.split(",");
        if (bcc.length > 0 && bcc[0].length() != 0) {
            set.addAll(Arrays.asList(bcc));
        }
        return set;
    }


}
