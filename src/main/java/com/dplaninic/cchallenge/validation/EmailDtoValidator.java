package com.dplaninic.cchallenge.validation;


import com.dplaninic.cchallenge.model.EmailDto;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class EmailDtoValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return EmailDto.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        EmailDto email = (EmailDto) obj;
        if (isEmptyString(email.from)) {
            errors.rejectValue("from", "from.email.empty");
            return;
        }
        if (isEmptyString(email.to)) {
            errors.rejectValue("to", "to.email.empty");
            return;
        }
        if(!isCommaSeparatedListOfEmailsValid(email.to)){
            errors.rejectValue("to", "to.email.invalid");
            return;
        }
        if(!isCommaSeparatedListOfEmailsValidOrEmpty(email.cc)){
            errors.rejectValue("cc", "cc.email.invalid");
            return;
        }
        if(!isCommaSeparatedListOfEmailsValidOrEmpty(email.bcc)){
            errors.rejectValue("bcc", "bcc.email.invalid");
            return;
        }
    }

    private boolean isEmptyString(String input) {

        return (input == null || input.trim().length() == 0);
    }

    private boolean isEmailValid(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    private boolean isCommaSeparatedListOfEmailsValid(String csv){
        String[] mails = csv.split(",");
        for (int i = 0; i < mails.length; i++) {
            if (!isEmailValid(mails[i])) {
                return false;
            }
        }
        return true;
    }

    private boolean isCommaSeparatedListOfEmailsValidOrEmpty(String csv){
        if (isEmptyString(csv)){
            return true;
        }
        return isCommaSeparatedListOfEmailsValid(csv);
    }


}