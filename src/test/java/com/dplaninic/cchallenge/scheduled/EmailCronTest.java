package com.dplaninic.cchallenge.scheduled;

import com.dplaninic.cchallenge.CchallengeApplication;
import com.dplaninic.cchallenge.model.Email;
import com.dplaninic.cchallenge.model.EmailBatch;
import com.dplaninic.cchallenge.model.EmailFactory;
import com.dplaninic.cchallenge.model.EmailIdentity;
import com.dplaninic.cchallenge.service.EmailBatchService;
import com.dplaninic.cchallenge.service.EmailBatchServiceImpl;
import com.dplaninic.cchallenge.service.EmailService;
import com.dplaninic.cchallenge.service.EmailServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {CchallengeApplication.class,
        SendGridEmailSender.class,
        EmailFactory.class,
        EmailCron.class,
        EmailBatchServiceImpl.class,
        EmailServiceImpl.class})
@TestPropertySource(properties = "scheduling.enabled=false")
public class EmailCronTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EmailFactory emailFactory;

    @Autowired
    private EmailCron emailCron;

    @MockBean
    EmailSender emailSender;

    @TestConfiguration
    static class EmailCronTestContextConfiguration {

        @Bean
        public EmailCron emailCron() {
            return new EmailCron();
        }

        @Bean
        public EmailFactory emailFactory() {
            return new EmailFactory();
        }

        @Bean
        public EmailService emailService() {
            return new EmailServiceImpl();
        }

        @Bean
        public EmailBatchService emailBatchService() {
            return new EmailBatchServiceImpl();
        }
    }

    @Before
    public void setUp() throws IOException {

        EmailBatch emailBatch = loadEmailBatchFromString();

        entityManager.persist(emailBatch);
        entityManager.flush();
        List<Email> emails = emailFactory.createEmailEntities(emailBatch.getDto(), emailBatch);
        emails.forEach(email -> email.setSent(true));

        Mockito.when(emailSender.sendEmails(Mockito.anyList())).thenReturn(emails);
    }

    private static final String path = "src/main/resources/test_load.json";

    private String loadFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    private EmailBatch loadEmailBatchFromString() throws IOException {
        String content = loadFromFile(path);
        return new EmailBatch(false, content);
    }

    @Test
    public void whenFindByUnexpanded_thenReturnEmails() throws IOException {

        EmailBatch emailBatchBefore = entityManager.find(EmailBatch.class, 1);
        assertEquals(emailBatchBefore.isExpanded(), false);
        Email emailBefore = entityManager.find(Email.class, new EmailIdentity(1,
                "dragan.planinic@gmail.com",
                "planinic.dragan@nsoft.com"));
        assertNull(emailBefore);

        emailCron.sendUnsentEmails();

        EmailBatch emailBatchAfter = entityManager.find(EmailBatch.class, 1);
        assertEquals(emailBatchAfter.isExpanded(), true);
        Email emailAfter = entityManager.find(Email.class, new EmailIdentity(1,
                "dragan.planinic@gmail.com",
                "planinic.dragan@nsoft.com"));
        assertNotNull(emailAfter);
        assertEquals(emailAfter.isSent(), true);

    }
}
