package com.dplaninic.cchallenge.scheduled;

import com.dplaninic.cchallenge.model.Email;

import java.util.List;

public interface EmailSender {

    public List<Email> sendEmails(List<Email> emailList);
}
