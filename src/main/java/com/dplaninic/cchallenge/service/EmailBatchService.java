package com.dplaninic.cchallenge.service;

import com.dplaninic.cchallenge.model.EmailBatch;

import java.util.List;

public interface EmailBatchService {

    public void save(final EmailBatch emailBatch);

    public List<EmailBatch> getAllUnexpanded();
}
