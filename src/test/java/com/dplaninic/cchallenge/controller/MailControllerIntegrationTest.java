package com.dplaninic.cchallenge.controller;

import com.dplaninic.cchallenge.model.EmailBatch;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestPropertySource(properties = "scheduling.enabled=false")
public class MailControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;
    private final ObjectMapper objectMapper = new ObjectMapper();

    private String loadFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    private EmailBatch loadEmailBatchFromString(String path) throws IOException {
        String content = loadFromFile(path);
        return new EmailBatch(false, content);
    }

    @Test
    public void postCreateEmailValid1() throws Exception {

        EmailBatch emailBatch = loadEmailBatchFromString("src/main/resources/json/valid/1.json");
        mvc.perform(MockMvcRequestBuilders.post("/createemail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailBatch.getDto())))
                .andExpect(status().isCreated());
    }

    @Test
    public void postCreateEmailValid2() throws Exception {

        EmailBatch emailBatch = loadEmailBatchFromString("src/main/resources/json/valid/2.json");
        mvc.perform(MockMvcRequestBuilders.post("/createemail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailBatch.getDto())))
                .andExpect(status().isCreated());
    }

    @Test
    public void postCreateEmailValid3() throws Exception {

        EmailBatch emailBatch = loadEmailBatchFromString("src/main/resources/json/valid/3.json");
        mvc.perform(MockMvcRequestBuilders.post("/createemail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailBatch.getDto())))
                .andExpect(status().isCreated());
    }

    @Test
    public void postCreateEmailValid4() throws Exception {

        EmailBatch emailBatch = loadEmailBatchFromString("src/main/resources/json/valid/4.json");
        mvc.perform(MockMvcRequestBuilders.post("/createemail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailBatch.getDto())))
                .andExpect(status().isCreated());
    }

    @Test
    public void postCreateEmailInvalid1() throws Exception {

        EmailBatch emailBatch = loadEmailBatchFromString("src/main/resources/json/invalid/1.json");
        mvc.perform(MockMvcRequestBuilders.post("/createemail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailBatch.getDto())))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postCreateEmailInvalid2() throws Exception {

        EmailBatch emailBatch = loadEmailBatchFromString("src/main/resources/json/invalid/2.json");
        mvc.perform(MockMvcRequestBuilders.post("/createemail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailBatch.getDto())))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postCreateEmailInvalid3() throws Exception {

        EmailBatch emailBatch = loadEmailBatchFromString("src/main/resources/json/invalid/3.json");
        mvc.perform(MockMvcRequestBuilders.post("/createemail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailBatch.getDto())))
                .andExpect(status().isBadRequest());
    }


}